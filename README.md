# Setup
Go to your `Startup.cs` in your `IdentityServer` project.
Change:
```csharp
services.AddIdentity<ApplicationUser, IdentityRole>()
```
To:
```csharp
services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(1);
                options.Lockout.MaxFailedAccessAttempts = 3;
            })
``` 
Here you can change the lockout settings.


To change password security edit:

```csharp
services.AddIdentity<ApplicationUser, IdentityRole>()
```

To: 
```csharp
services.AddIdentity<ApplicationUser, IdentityRole>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;

                // Changed this to 8 to add better security.
                options.Password.RequiredLength = 8;
                options.Password.RequiredUniqueChars = 2;
            })
```


In the solution of IdentityServer there is a folder located named `Quickstart`. Next go into `Account` to your `AccountController.cs`.
In your Login method add the next lines of code underneath:
```csharp 
if(result.Succeeded){
    ...
}
```

This:
```csharp
if (result.IsLockedOut)
{
    return View("LockedOut");
}
```
This condition will show a view for the user that he/she has been locked out.

In `Views/Account/` add a new file named `LockedOut.cshtml`.

You can fill in the next code for example:

```html
<div class="page-header locked-out">
    <h1>
        Locked out
        <small>You are now locked out</small>
    </h1>
</div>

```

